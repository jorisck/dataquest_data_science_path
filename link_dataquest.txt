https://regexr.com/

the secret to winning Kaggle competitions: 
https://www.import.io/post/how-to-win-a-kaggle-competition/

Not All Practice Makes Perfect: 
http://nautil.us/issue/35/boundaries/not-all-practice-makes-perfect

An In-Depth Style Guide for Data Science Projects: 
https://www.dataquest.io/blog/data-science-project-style-guide/

What is a GPU and do you need one in Deep Learning? : 
https://towardsdatascience.com/what-is-a-gpu-and-do-you-need-one-in-deep-learning-718b9597aa0d

gitignore/Python.gitignore: 
https://github.com/github/gitignore/blob/master/Python.gitignore

Neural network architectures
https://www.asimovinstitute.org/neural-network-zoo/

Google style docstrings	
http://google.github.io/styleguide/pyguide.html#38-comments-and-docstrings

Shuffle dans Spark, reduceByKey vs groupByKey
https://blog.univalence.io/shuffle-dans-spark-reducebykey-vs-groupbykey/

Top 20 Apache Spark Interview Questions 2019
https://acadgild.com/blog/top-20-apache-spark-interview-questions-2019

Install PySpark to run in Jupyter Notebook on Windows
https://naomi-fridman.medium.com/install-pyspark-to-run-on-jupyter-notebook-on-windows-4ec2009de21f

yield statement in Python
http://stackoverflow.com/a/231855

SPaRK core
http://spark.apache.org/docs/latest/api/python/pyspark.html

Visual representation of methods (IPython Notebook format)
https://nbviewer.jupyter.org/github/jkthompson/pyspark-pictures/blob/master/pyspark-pictures.ipynb

Spark SQL Data Sources API: 
https://databricks.com/blog/2015/01/09/spark-sql-data-sources-api-unified-data-access-for-the-spark-platform.html

World record set for 100 TB sort by open source and public cloud team:
https://opensource.com/business/15/1/apache-spark-new-world-record

Feature Selection For Machine Learning in Python
https://machinelearningmastery.com/feature-selection-machine-learning-python/

history of deep neural networks
https://arxiv.org/ftp/arxiv/papers/1803/1803.01164.pdf

the United States Post Office used handwriting recognition software
http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.852.5499&rep=rep1&type=pdf

Notes - Cross Validation:
The standard deviation of the RMSE values can be a proxy for a model's variance 
while the average RMSE is a proxy for a model's bias. 
Bias and variance are the 2 observable sources of error in a model that we can indirectly control.

https://scikit-learn.org/stable/modules/cross_validation.html#cross-validation

The 5 Levels of Machine Learning Iteration
https://elitedatascience.com/machine-learning-iteration#micro

Why is machine learning 'hard'?
http://ai.stanford.edu/~zayd/why-is-machine-learning-hard.html

