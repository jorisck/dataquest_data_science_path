{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "# Project: Predicting Bike Rentals\n",
    "\n",
    "## Context\n",
    "Many American cities have communal bike sharing stations where you can rent bicycles by the hour or day. Washington, D.C. is one of these cities. The District collects detailed data on the number of bicycles people rent by the hour and day.\n",
    "\n",
    "## Data\n",
    "You can download the data from the [University of California, Irvine's website](http://archive.ics.uci.edu/ml/datasets/Bike+Sharing+Dataset)\n",
    "The file contains 17380 rows, with each row representing the number of bike rentals for a single hour of a single day.\n",
    "\n",
    "Here are the descriptions for the relevant columns:\n",
    "\n",
    "* instant - A unique sequential ID number for each row\n",
    "* dteday - The date of the rentals\n",
    "* season - The season in which the rentals occurred\n",
    "* yr - The year the rentals occurred\n",
    "* mnth - The month the rentals occurred\n",
    "* hr - The hour the rentals occurred\n",
    "* holiday - Whether or not the day was a holiday\n",
    "* weekday - The day of the week (as a number, 0 to 7)\n",
    "* workingday - Whether or not the day was a working day\n",
    "* weathersit - The weather (as a categorical variable)\n",
    "* temp - The temperature, on a 0-1 scale\n",
    "* atemp - The adjusted temperature\n",
    "* hum - The humidity, on a 0-1 scale\n",
    "* windspeed - The wind speed, on a 0-1 scale\n",
    "* casual - The number of casual riders (people who hadn't previously signed up with the bike sharing program)\n",
    "* registered - The number of registered riders (people who had already signed up)\n",
    "* cnt - The total number of bike rentals (casual + registered)\n",
    "\n",
    "## Goal\n",
    "In this project, we'll try to predict the total number of bikes people rented in a given hour. \n",
    "\n",
    "We'll predict the **cnt** column using all of the other columns, except for **casual** and **registered**. To accomplish this, we'll create a few different machine learning models and evaluate their performance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# import libs\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# import data\n",
    "bike_rentals=pd.read_csv(\"bike_rental_hour.csv\")\n",
    "bike_rentals.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "bike_rentals['cnt'].hist()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We don't have any special observations here. We have the higher y-value between 0 and 100. After that the y-value is decreasing. \n",
    "So he higher the total bicycle rentals, the fewer rows we have with that value"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# explore correlation of each column with cnt\n",
    "corr=bike_rentals.corr()\n",
    "corr['cnt'].sort_values(ascending=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we have a positive correlation with temp & hr and a negative correlation with hum. That make sense because peaple like riding bicycles when the wheather is nice and at a specific time of day. While a lot of humidy in the air is not a good condition for people to use bicycle."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Calculating Features\n",
    "It can often be helpful to calculate features before applying machine learning models. Features can enhance the accuracy of models by introducing new information, or distilling existing information."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "bike_rentals['hr'].unique()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here the **hr** column in bike_rentals contains the hours during which bikes are rented, from 1 to 24.\n",
    "\n",
    "A machine will treat each hour differently, without understanding that certain hours are related. We can introduce some order into the process by creating a new column with labels for morning, afternoon, evening, and night. This will bundle similar times together, enabling the model to make better decisions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def assign_label(hour):\n",
    "    \"\"\"Divides a day in 4 categories and returns the category\n",
    "    to which the 'hour'belongs\n",
    "    \n",
    "    Args: \n",
    "        hour(int): the hour\n",
    "    \n",
    "    Returns:\n",
    "        category (int) : a number between 1 and 4 - \n",
    "                        1 for morning, \n",
    "                        2 for afternoon,\n",
    "                        3 for evening\n",
    "                        4 for night\n",
    "    \"\"\"\n",
    "    \n",
    "    if hour>=6 and hour<12:\n",
    "        return 1\n",
    "    elif hour>=12 and hour <18:\n",
    "        return 2\n",
    "    elif hour>=18 and hour < 24:\n",
    "        return 3\n",
    "    else:\n",
    "        return 4"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# apply the assign_label to the 'hr' column\n",
    "bike_rentals['time_label']=bike_rentals['hr'].apply(assign_label)\n",
    "bike_rentals[['hr', 'time_label']].head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Splitting the Data Into Train and Test Sets\n",
    "Before we can begin applying machine learning algorithms, we'll need to split the data into training and testing sets. This will enable us to train an algorithm using the training set, and evaluate its accuracy on the testing set. If we train an algorithm on the training data, then evaluate its performance on the same data, we can get an unrealistically low error value, due to overfitting.\n",
    "\n",
    "To evaluate te accuracy we'll use the [RMSE](https://medium.com/human-in-a-machine-world/mae-and-rmse-which-metric-is-better-e60ac3bde13d). Because large errors in prediction are particularly undesirable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# import an error metric to evaluate the performance of the ML algo\n",
    "from sklearn.metrics import mean_squared_error\n",
    "\n",
    "# Select 80% of the rows for trainning\n",
    "train = bike_rentals.sample(frac=0.8)\n",
    "test = bike_rentals.loc[~bike_rentals.index.isin(train.index)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Applying Linear Regression\n",
    "As we know, linear regression works best when predictors are linearly correlated to the target and also independent -- in other words, they don't change meaning when we combine them with each other. The good thing about linear regression is that it's fairly resistant to overfitting because it's straightforward. It also can be prone to underfitting the data, however, and not building a powerful enough model. This means that linear regression usually isn't the most accurate option."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Create a list of predictor columns to use in training and predictions\n",
    "predictor = list(train.columns)\n",
    "predictor.remove('cnt')\n",
    "predictor.remove('casual')\n",
    "predictor.remove('dteday')\n",
    "predictor.remove('registered')\n",
    "\n",
    "predictor"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from sklearn.linear_model import LinearRegression\n",
    "\n",
    "reg=LinearRegression()\n",
    "\n",
    "reg.fit(train[predictor], train['cnt'])\n",
    "predictions=reg.predict(test[predictor])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Calculate the error\n",
    "mse=mean_squared_error(test['cnt'], predictions)\n",
    "rmse=np.sqrt(mse)\n",
    "\n",
    "print('The mse value is {}.\\nThe rmse value is {}'.format(mse,rmse))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As the mean squared error value for a single model isn't all that useful we also calculated the RMSE.\n",
    "\n",
    "Our model achieved an RMSE value of approximately 130.5, which implies that we should expect for the model to be off by 130.5 dollars on average for the predicted count values. Given that most of the number of bike rentals are listed in the majority at a few hundred count, we need to reduce this error as much as possible to improve the model's usefulness."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Applying Decision Trees\n",
    "Now we're ready to apply the decision tree algorithm. We'll be able to compare its error with the error from linear regression, which will enable us to pick the right algorithm for this data set.\n",
    "\n",
    "\n",
    "Decision trees tend to predict outcomes much more reliably than linear regression models. Because a decision tree is a fairly complex model, it also tends to overfit, particularly when we don't tweak parameters like maximum depth and minimum number of samples per leaf. Decision trees are also prone to instability -- small changes in the input data can result in a very different output model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from sklearn.tree import DecisionTreeClassifier\n",
    "\n",
    "tree=DecisionTreeClassifier()\n",
    "tree.fit(train[predictor],train['cnt'])\n",
    "\n",
    "tree_predictions=tree.predict(test[predictor])\n",
    "\n",
    "mse_t=mean_squared_error(test['cnt'],tree_predictions)\n",
    "rmse_t=np.sqrt(mse_t)\n",
    "\n",
    "print('The mse value is {}.\\nThe rmse value is {}'.format(mse_t,rmse_t))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The values for the mse and the rmse are very good. We can supect an overfitting here. We'll tweak parameter min_samples_leaf (The minimum number of samples required to be at a leaf node) to be more confident about our results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "tree=DecisionTreeClassifier(min_samples_leaf=5)\n",
    "tree.fit(train[predictor],train['cnt'])\n",
    "tree_predictions=tree.predict(test[predictor])\n",
    "mse_t=mean_squared_error(test['cnt'],tree_predictions)\n",
    "rmse_t=np.sqrt(mse_t)\n",
    "\n",
    "print('The mse value is {}.\\nThe rmse value is {}'.format(mse_t,rmse_t))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "tree=DecisionTreeClassifier(min_samples_leaf=2)\n",
    "tree.fit(train[predictor],train['cnt'])\n",
    "tree_predictions=tree.predict(test[predictor])\n",
    "mse_t=mean_squared_error(test['cnt'],tree_predictions)\n",
    "rmse_t=np.sqrt(mse_t)\n",
    "\n",
    "print('The mse value is {}.\\nThe rmse value is {}'.format(mse_t,rmse_t))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By taking the nonlinear predictors into account, the decision tree regressor appears to have much higher accuracy than linear regression."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Applying Random Forests\n",
    "We can now apply the random forest algorithm, which improves on the decision tree algorithm. Random forests tend to be much more accurate than simple models like linear regression. Due to the way random forests are constructed, they tend to overfit much less than decision trees. Random forests can still be prone to overfitting, though, so it's important to tune parameters like maximum depth and minimum samples per leaf"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from sklearn.ensemble import RandomForestRegressor\n",
    "\n",
    "forest= RandomForestRegressor()\n",
    "forest.fit(train[predictor], train['cnt'])\n",
    "predctions_f=forest.predict(test[predictor])\n",
    "mse_f=mean_squared_error(test['cnt'],predctions_f)\n",
    "rmse_f=np.sqrt(mse_f)\n",
    "\n",
    "print('The mse value is {}.\\nThe rmse value is {}'.format(mse_f,rmse_f))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from sklearn.ensemble import RandomForestRegressor\n",
    "\n",
    "forest= RandomForestRegressor(min_samples_leaf=5)\n",
    "forest.fit(train[predictor], train['cnt'])\n",
    "predctions_f=forest.predict(test[predictor])\n",
    "mse_f=mean_squared_error(test['cnt'],predctions_f)\n",
    "rmse_f=np.sqrt(mse_f)\n",
    "\n",
    "print('The mse value is {}.\\nThe rmse value is {}'.format(mse_f,rmse_f))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By removing some of the sources of overfitting, the random forest accuracy is improved over the decision tree accuracy."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Next Steps\n",
    "\n",
    "Here are some potential next steps:\n",
    "\n",
    "* Calculate additional features, such as:\n",
    "    * An index combining temperature, humidity, and wind speed\n",
    "* Try predicting casual and registered instead of cnt."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.4.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
