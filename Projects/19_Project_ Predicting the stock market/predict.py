import pandas as pd
import numpy as np
from datetime import datetime

from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error,  mean_squared_error

df=pd.read_csv('sphist.csv', parse_dates=['Date'])

print(df.head(), '\n')

date_gt_test = df["Date"] > datetime(year=2015, month=4, day=1)

print(date_gt_test.iloc[0:5], '\n')

df=df.sort_values(by='Date', ascending = True)

print(df.head(), '\n')

# Create indicators

"""
In a normal machine learning exercise, we treat each row as independent. Stock market data is sequential, and each observation comes a day after the previous observation. Thus, the observations are not all independent, and you can't treat them as such.

The time series nature of the data means that we can generate indicators to make our model more accurate. For instance, you can create a new column that contains the average price of the last 10 trades for each row. This will incorporate information from multiple prior rows into one, and will make predictions much more accurate.

The rolling mean will use the current day's price. You'll need to reindex the resulting series to shift all the values "forward" one day.
We'll need to reindex the resulting series to shift all the values "forward" one day
"""

df['avg_5']=df['Close'].rolling(5).mean().shift(1)
df['avg_30']=df['Close'].rolling(30).mean().shift(1)
df['avg_365']=df['Close'].rolling(365).mean().shift(1)

df['avg_5/avg_365']=df['avg_5']/df['avg_365']

df['std_5']=df['Close'].rolling(5).std().shift(1)
df['std_30']=df['Close'].rolling(30).std().shift(1)
df['std_365']=df['Close'].rolling(365).std().shift(1)

df['std_5/std_365']=df['std_5']/df['std_365']

df['avg_5_v']=df['Volume'].rolling(5).mean().shift(1)
df['avg_365_v']=df['Volume'].rolling(365).mean().shift(1)
df['avg_5_v/avg_365_v']=df['avg_5_v']/df['avg_365_v']

"""
Some of the indicators use 365 days of historical data, and the dataset starts on 1950-01-03. Thus, any rows that fall before 1951-01-03 don't have enough historical data to compute all the indicators. You'll need to remove these rows before you split the data.
"""

df=df[df["Date"] > datetime(year=1951, month=1, day=3)]

# Remove any rows with NaN values
df.dropna(axis=0, inplace=True) # argument to drop rows
print(df.head(), '\n')

"""
Train should contain any rows in the data with a date less than 2013-01-01. test should contain any rows with a date greater than or equal to 2013-01-01.
"""

train_df=df[df["Date"]< datetime(year=2013, month=1, day=1)]

test_df=df[df["Date"]>= datetime(year=2013, month=1, day=1)]

print(train_df["Date"].tail(), '\n')
print(test_df["Date"].head(), '\n')

# Make predictions with LinearRegression

"""
Train a linear regression model, using the train Dataframe. Leave out all of the original columns (Close, High, Low, Open, Volume, Adj Close, Date) when training your model. These all contain knowledge of the future that we don't want to feed the model. The Close column will be the target.
"""
cols=["avg_5", "avg_30", "avg_365", "std_5", "std_365", "avg_5/avg_365", "std_5/std_365"]

lr= LinearRegression()
lr.fit(train_df[cols], train_df['Close'])
predictions=lr.predict(test_df[cols])

# Calculate error metrics

"""
It's recommended to use Mean Absolute Error, also called MAE, as an error metric, because it will show you how "close" you were to the price in intuitive terms.
"""
mae= mean_absolute_error(test_df['Close'],predictions)

"""
Mean Squared Error, or MSE, is an alternative that is more commonly used, but makes it harder to intuitively tell how far off you are from the true price because it squares the error.
"""
mse= mean_squared_error(test_df['Close'], predictions)

print("This is the mae {} and the mse {}".format(mae, mse))

"""
Add 2 additional indicators to your dataframe, and see if the error is reduced:
* The average volume over the past five days.
* The average volume over the past year.
* The ratio between the average volume for the past five days, and the average volume for the past year.
"""

cols=["avg_5", "avg_30", "avg_365", "std_5", "std_365", "avg_5/avg_365", "std_5/std_365","avg_5_v","avg_365_v",
      "avg_5_v/avg_365_v"]

# Make predictions with LinearRegression

lr= LinearRegression()
lr.fit(train_df[cols], train_df['Close'])
predictions=lr.predict(test_df[cols])

# Calculate error metrics

mae= mean_absolute_error(test_df['Close'],predictions)
mse= mean_squared_error(test_df['Close'], predictions)

print("This is the new mae {} and the new mse {}".format(mae, mse))